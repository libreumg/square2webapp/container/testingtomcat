ARG TOMCAT_VERSION=9.0.52-jre11-openjdk-slim-bullseye

FROM tomcat:$TOMCAT_VERSION

ARG SQ2_VERSION=

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-java&name=Square2&sort=version&direction=desc&version=$SQ2_VERSION" /tmp/war_meta
RUN apt-get -y update ; apt-get -y install curl gawk jq zip 


RUN mv /usr/local/tomcat/webapps.dist/* /usr/local/tomcat/webapps/
#RUN export curr_sq2=$(jq -r '.items[0].version' /tmp/war_meta) && \
#    curl "https://packages.qihs.uni-greifswald.de/repository/ship-snapshot-java/war/Square2/$curr_sq2/Square2-$curr_sq2.war" --output /usr/local/tomcat/webapps/Square2.war
RUN mkdir /etc/tomcatsquare2/

COPY square2.properties /etc/tomcatsquare2/square2.properties
COPY tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml
COPY context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml
COPY host_logs.sh /host_logs.sh
COPY start.sh /start.sh
RUN chmod 777 /host_logs.sh && chmod 777 /start.sh
RUN mkdir /usr/local/tomcat/webapps/logs
EXPOSE 8080
CMD /start.sh
