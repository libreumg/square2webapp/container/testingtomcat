#!/bin/bash
while true; do
    cd /usr/local/tomcat/logs
    zip -j logs.zip ./* /opt/logs/* > /dev/null
    cp -r logs.zip /usr/local/tomcat/webapps/logs/logs.zip
    sleep 2
done
